"""Workflow routes module."""

from flask import request, Blueprint
from services.workflow_service import getAllWorkflows, createWorkflow, setWorkflowStatus
from http import HTTPStatus
from services.message_queue import pushToQueue, consume
from flask import  request

workflow_routes = Blueprint('wotkflow_api', __name__)

@workflow_routes.route('/workflow/', methods=['POST', 'GET'])
def worflow_post_and_get():
	"""
	Route to create and get all workflows.
	This route creates a new workflow and sends a message 
	to the queue or gets all workflows.
	"""
	
	if request.method == 'POST':
		result = createWorkflow(request.json)
		pushToQueue(result['uuid'])
		return result, HTTPStatus.CREATED

	elif request.method ==  'GET':
		result = getAllWorkflows()
		return { 'workflows': result }

	return {'msg': 'GET OR CREATE'}

@workflow_routes.route('/workflow/consume', methods=['POST'])
def worflow_process():
	"""
	Route to consume workflow.
	"""
	consume()
	return {'msg': 'Cosuming'}

@workflow_routes.route('/workflow/<uuid>', methods=['PATCH'])
def worflow_update(uuid):
	"""
	Route to set Workflow status.
	If the status is inserted it will change to 
	consumed, if it is consumed it will change 
	to inserted.
	"""
	try:
		workflow = setWorkflowStatus(uuid)

		if workflow['status'] == 'inserted':
			pushToQueue(workflow['uuid'])

	except Exception as e:
		return {'msg': str(e)}, HTTPStatus.NOT_FOUND

	return {'msg': 'Workflow updated'}
