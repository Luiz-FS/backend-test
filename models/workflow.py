"""Workflow Model."""

import uuid
from sqlalchemy.dialects.postgresql import JSON, ARRAY, UUID
from dataclasses import dataclass
from database import db

@dataclass
class Workflow(db.Model):
    """
    Class Workflow Model.

    Attributs:
        - id(UUID): Workflow id.
        - status(ENUM): Workflow Status.
        - data(JSON): Workflow data.
        - steps(ARRAY): Workflow steps.
    """
    __tablename__ = 'workflow'
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    status = db.Column(db.Enum('inserted', 'consumed', name='status'), default='inserted')
    data = db.Column(JSON)
    steps = db.Column(ARRAY(db.String))

    def to_dict(self):
        """
        Method to create dict representation of workflow.

        Returns: Dict with format {
            'uuid': ...,
            'status': ...,
            'data': {...},
            'steps': [...]
        }
        """
        return {
            'uuid': str(self.id),
            'status': self.status,
            'data': self.data,
            'steps': self.steps
        }