"""Main module."""

from config import app
from routes.workflow_routes import workflow_routes
from database import db

db.create_all()
app.register_blueprint(workflow_routes)
