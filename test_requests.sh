#!/bin/bash

echo "Posting Workflow..."

resp=$(curl -X POST \
  http://localhost:8080/workflow/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 9f55d927-dbfd-a401-1366-13d2af8fb998' \
  -d '{
  "data": {
    "teste": "ola",
    "oi": "teste"
  },
  "steps": [
    "Teste step 1",
    "teste step 2",
    "teste step 3"
  ]
}')

echo -e "$resp\n\n\n\n"

uuid=$(echo $resp | egrep -o '"uuid":".*"' | awk -F: '{print $2}' | egrep -o "[^\"]+")

echo "Consuming Workflow..."

curl -X POST \
  http://localhost:8080/workflow/consume \
  -H 'cache-control: no-cache' \
  -H 'postman-token: e9cfc12b-3122-df36-9a5d-05b5015ba20f'

echo -e "\n\n\n\nGet all Workflows..."

curl -X GET \
  http://localhost:8080/workflow/ \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 29e68d74-53fd-97c6-cdb2-74cbd1d9c91a'


echo -e "\n\n\n\nSet workflow status"
curl -X PATCH \
  http://localhost:8080/workflow/$uuid \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 8c65cff8-a42d-a941-7b5f-e5552babc1e5'


echo -e "\n\n\n\nGet all Workflows..."

curl -X GET \
  http://localhost:8080/workflow/ \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 29e68d74-53fd-97c6-cdb2-74cbd1d9c91a'
