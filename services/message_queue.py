"""Message Queue Service."""

import pika
import json
import threading
import csv
import os
from models.workflow import Workflow
from database import db
from dotenv import load_dotenv

load_dotenv()

connection = pika.BlockingConnection(pika.ConnectionParameters(os.getenv('RABBITMQ_HOST')))
channel = connection.channel()

channel.queue_declare(queue='work_consume')

def pushToQueue(data):
    """
    Method for push message to queue.

    Arguments:
        - data: String messagem to send.
    """
    channel.basic_publish(exchange='', routing_key='work_consume', body=data)

def consumeQueue(ch, method, properties, body):
    """
    Method for consume and process queue messages.

    Arguments:
        - ch: Connection channel.
        - method: Message queue method.
        - properties: Message queue properties.
        - body: Message to be processed.
    """
    uuid = body.decode('utf-8')
    workflow = Workflow.query.get(uuid)

    if workflow.status == 'inserted':
        row = [[uuid, workflow.data]]
        workflow.status = 'consumed'
        db.session.commit()

        with open('data/workflows.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerows(row)

    ch.basic_ack(delivery_tag=method.delivery_tag)
    ch.basic_cancel(consumer_tag="csn")

def consumeThread():
    """
    Method to create and start consumer connection.
    """
    connection2 = pika.BlockingConnection(pika.ConnectionParameters(os.getenv('RABBITMQ_HOST')))
    channel2 = connection2.channel()
    channel2.queue_declare(queue='work_consume')
    channel2.basic_consume(queue='work_consume', consumer_tag="csn", on_message_callback=consumeQueue, auto_ack=False)
    channel2.start_consuming()

def consume():
    """
    Method to create and start consumer thread.
    """
    thr = threading.Thread(target=consumeThread)
    thr.start()
    thr.join(timeout=2)
