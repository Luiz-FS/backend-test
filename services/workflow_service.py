"""Workflow Service."""

from database import db
from models.workflow import Workflow
import uuid

def createWorkflow(data):
    """
    Method to create workflow

    Arguments:
        - data: Dict object with Workflow attributes.
    
    Returns: Dict representantion of workflow.
    """
    workflow = Workflow(data=data['data'], steps=data['steps'])
    db.session.add(workflow)
    db.session.commit()
    return workflow.to_dict()

def getAllWorkflows():
    """
    Method to get all Workflows.

    Returns: List of all workflows.
    """
    workflows = Workflow.query.all()
    return list(map(lambda workflow: workflow.to_dict(), workflows))

def setWorkflowStatus(workflow_uuid):
    """
    Method to set workflow status.

    Arguments:
        - workflow_uuid: uuid of workflow.

    Returns: Updated workflow.
    """
    try: 
        workflow = Workflow.query.get(workflow_uuid)
        workflow.status = 'consumed' if workflow.status == 'inserted' else 'inserted'
        db.session.commit()

        return workflow.to_dict()
    except Exception:
        raise Exception('Workflow not found!')

