"""Database module."""

import psycopg2
import json
from config import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)
