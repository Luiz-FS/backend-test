"""Server configs module."""

import os
from flask import Flask, request
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('POSTGRE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

